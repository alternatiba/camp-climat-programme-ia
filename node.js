module.exports = class Node {
  constructor(config, stake, resources, solutions, deep) {
    // console.log('config',config);
    this.config = config;
    this.stake = stake;
    this.resources = resources;
    this.solutions = solutions;
    this.deep = deep;
    // this.index=index||0;
    // this.candidates = this.stake.filter(r => r.proced != true);
    this.status = "pending";
    let roomAvailabilityStart;
    let roomAvailabilityEnd;
    this.resources.roomAvailability.forEach(ra => {
      if ((roomAvailabilityStart == undefined) || (ra.startAvailability.getTime() < roomAvailabilityStart.getTime())) {
        roomAvailabilityStart = ra.startAvailability;
      }
      if ((roomAvailabilityEnd == undefined) || (ra.endAvailability.getTime() > roomAvailabilityEnd.getTime())) {
        roomAvailabilityEnd = ra.endAvailability;
      }
    });
    this.roomAvailabilityStart = roomAvailabilityStart;
    this.roomAvailabilityEnd = roomAvailabilityEnd;
    this.roomAvailabilityLength = roomAvailabilityEnd.getTime() - roomAvailabilityStart.getTime();
    this.candidate = this.stake[this.deep];
    this.computeHeurisic()
  }

  // skip(){
  //   this.deep++;
  //   this.candidate = this.stake[this.deep];
  // }

  takeStep() {
    // console.log('phase 1');
    // console.log('--------------');
    if (this.deep < this.stake.length) {



      let roomAvailabilityCandidates = this.resources.roomAvailability.filter(r => r.volume >= this.candidate.volume);
      //console.log('roomAvailabilityCandidates',roomAvailabilityCandidates);
      roomAvailabilityCandidates.sort((a, b) => a.volume - b.volume);
      // let index = 0;
      // let roomAvailabilityChoose = roomAvailabilityCandidates[index];

      // let lastSameTrainingOccurence = this.solutions.filter(s => s.key == this.candidate.key).sort((a, b) => b.occurence > a.occurence)[0];
      let slotAvailability = [];
      let slotDisable = [];
      // console.log('slotsAvaible',this.candidate.slotsAvaible.length);
      // console.log('solutions',this.solutions.length);
      this.candidate.slotsAvaible.forEach(sa => {
        let conflict = undefined;
        this.solutions.forEach(solution => {
          let startSolutionTime = solution.start;
          // solution.start=startSolutionTime;
          let endSolutionTime = solution.end;
          // solution.end=endSolutionTime;

          //console.log('solution',solution);
          if (this.candidate.dependencies != undefined && this.candidate.dependencies.length > 0) {
            if (this.candidate.dependencies.map(d => d.dependency).includes(solution.id) && endSolutionTime.getTime() >= sa.startStepTime.getTime()) {
              conflict = 'dependencyBefore';
              slotDisable.push({
                id: this.candidate.id,
                key: this.candidate.key,
                startStepTime: sa.startStepTime,
                endStepTime: sa.endStepTime,
                dependencies: JSON.stringify(this.candidate.dependencies.map(d => d.dependency)),
                conflict: 'dependencyBefore'
              });
            }
          }

          if (this.candidate.reverseDependencies != undefined && this.candidate.reverseDependencies.length > 0) {
            if (this.candidate.reverseDependencies.map(d => d.dependency).includes(solution.id) && startSolutionTime.getTime() <= sa.endStepTime.getTime()) {
              conflict = 'dependencyAfter';
              slotDisable.push({
                id: this.candidate.id,
                key: this.candidate.key,
                startStepTime: sa.startStepTime,
                endStepTime: sa.endStepTime,
                reverseDependencies: JSON.stringify(this.candidate.reverseDependencies.map(d => d.dependency)),
                conflict: 'dependencyAfter'
              });
            }
          }


          if (!(sa.startStepTime.getTime() > endSolutionTime.getTime() || sa.endStepTime.getTime() < startSolutionTime.getTime())) {
            // console.log('CONFLICT ?');
            if (
              (solution.keyRoom == sa.keyRoom)
            ) {
              // console.log('CONFLICT ROOM',sa.keyRoom);
              conflict = 'room';
              slotDisable.push({
                id: this.candidate.id,
                key: this.candidate.key,
                startStepTime: sa.startStepTime,
                endStepTime: sa.endStepTime,
                keyRoom: sa.keyRoom,
                keyTrainer: JSON.stringify(sa.keyTrainer),
                conflict: 'room',
                slotConflict: solution.id,
              });
            }
            let conflictTrainer = false;
            let conflictTrainerDetail = [];
            if(solution.inscriptionFree==undefined && sa.inscriptionFree==undefined){
              for (let solutionTrainer of solution.keyTrainer) {
                for (let saTrainer of sa.keyTrainer) {
                  if (saTrainer == solutionTrainer) {
                    // console.log('CONFLICT TRAINER DETAIL',saTrainer,solutionTrainer);
                    conflictTrainer = true;
                    conflictTrainerDetail.push(solution);
                  }
                }
              }
            }
            if (
              conflictTrainer == true
            ) {
              // console.log('CONFLICT !');
              // console.log('CONFLICT Trainer',sa.keyTrainer);
              conflict = 'trainer';
              conflictTrainerDetail.forEach(ctd => {
                // console.log('ctd',ctd);
                slotDisable.push({
                  id: this.candidate.id,
                  key: this.candidate.key,
                  startStepTime: sa.startStepTime,
                  endStepTime: sa.endStepTime,
                  keyRoom: sa.keyRoom,
                  keyTrainer: JSON.stringify(sa.keyTrainer),
                  conflict: 'trainer',
                  slotConflict: ctd.id,
                });
              })
            }
          }
        })
        // console.log('conflict',conflict);
        if (conflict == undefined) {
          // console.log('--',sa.keyTrainer);
          slotAvailability.push({
            startStepTime: sa.startStepTime,
            endStepTime: sa.endStepTime,
            keyRoom: sa.keyRoom,
            keyTrainer: sa.keyTrainer,
            // roomAvailability: sa.roomAvailability,
            // trainerAvailability: sa.trainerAvailability,
            roomStartAvailability: sa.roomStartAvailability,
          });
        } else {


          // console.log('conflictObject',conflictObject);
          //console.log('CONFLICT',startSolutionTime,endSolutionTime,r.keyRoom,ta.keyTrainer,startStepTime,endStepTime);
        }
      })
      // console.log('slotAvailability',slotAvailability);

      // let slotAvailability = [];
      // // console.log('phase 2');
      // roomAvailabilityCandidates.forEach(r => {
      //   let startRoomTime = r.startAvailability;
      //   let endRoomTime = r.endAvailability;
      //   let lengthRoomTime = (endRoomTime - startRoomTime) / 1000 / 60;
      //   let slotStepNb = (lengthRoomTime / 30);
      //
      //   // console.log('startRoomTime',startRoomTime);
      //   // console.log('endRoomTime',endRoomTime);
      //   // console.log('lengthRoomTime',lengthRoomTime);
      //   // console.log('slotStepNb',slotStepNb);
      //   for (let startStep = 0; startStep < slotStepNb; startStep++) {
      //     let startStepTime = new Date(startRoomTime.getTime() + (startStep * this.config.lengthMinuteStep * 60 * 1000));
      //     let endStepTime = new Date(startStepTime.getTime() + (this.candidate.length * 60 * 1000));
      //
      //     if (endStepTime <= endRoomTime) {
      //       let trainerAvailabilityValid = this.resources.trainerAvailability.filter(ta =>
      //         ta.affectation.filter(a =>
      //           a == this.candidate.key
      //         ).length > 0 &&
      //         ta.startAvailability.getTime() <= startStepTime.getTime() && ta.endAvailability.getTime() >= endStepTime.getTime()
      //       );
      //       // console.log("trainerAvailabilityValid + roomAvailability",trainerAvailabilityValid.length,r.keyRoom,startStepTime,endStepTime);
      //       trainerAvailabilityValid.forEach(ta => {
      //         let conflict = false;
      //         this.solutions.forEach(solution => {
      //           let startSolutionTime = solution.start;
      //           // solution.start=startSolutionTime;
      //           let endSolutionTime = solution.end;
      //           // solution.end=endSolutionTime;
      //
      //           //console.log('solution',solution);
      //           if (!(startStepTime.getTime() > endSolutionTime.getTime() || endStepTime.getTime() < startSolutionTime.getTime())) {
      //             if (
      //               (solution.keyRoom == r.keyRoom) ||
      //               (solution.keyTrainer == ta.keyTrainer)
      //             ) {
      //               conflict = true;
      //               // console.log('CONFLICT :',startSolutionTime,endSolutionTime,r.keyRoom,ta.keyTrainer,startStepTime,endStepTime);
      //             }
      //             // console.log(solution.keyTrainer,ta.keyTrainer,conflict)
      //           }
      //
      //           // if ((solution.keyRoom == r.keyRoom) && (!(startStepTime.getTime() > endSolutionTime.getTime() || endStepTime.getTime() < startSolutionTime.getTime()))) {
      //           //   conflict = true;
      //           //   // console.log('CONFLICT',r.keyRoom,startStepTime,endStepTime,solution.start,solution.end);
      //           // }
      //         })
      //         if (conflict == false) {
      //           slotAvailability.push({
      //             startStepTime: startStepTime,
      //             endStepTime: endStepTime,
      //             room: r.keyRoom,
      //             trainer: ta.keyTrainer,
      //             roomAvailability: r,
      //             trainerAvailability: ta
      //           });
      //         } else {
      //           //console.log('CONFLICT',startSolutionTime,endSolutionTime,r.keyRoom,ta.keyTrainer,startStepTime,endStepTime);
      //         }
      //
      //       })
      //     }
      //   }
      // })
      // console.log('phase 3',slotAvailability.length);
      // console.log('slotAvailability:', slotAvailability.map(r=>r.startStepTime.toISOString?r.startStepTime.toISOString():r.startStepTime+'-'+r.endStepTime.toISOString?r.endStepTime.toISOString():r.endStepTime+':'+r.room));
      this.childs = [];
      let childCadidates = [];
      //this.candidate.proced = true;
      if (slotAvailability.length > 0) {
        // let slotAvailabilityChose = slotAvailability[0];
        //console.log(slotAvailability);

        slotAvailability.forEach(slotAvailabilityChose => {
          // console.log('slotAvailabilityChose',slotAvailabilityChose);
          // let solution = JSON.parse(JSON.stringify(this.candidate));
          let solution = {
            id: this.candidate.id,
            key: this.candidate.key,
            keyRoom: slotAvailabilityChose.keyRoom,
            keyTrainer: slotAvailabilityChose.keyTrainer,
            start: slotAvailabilityChose.startStepTime,
            end: slotAvailabilityChose.endStepTime,
            roomStartAvailability: slotAvailabilityChose.roomStartAvailability,
            keyDomain: this.candidate.keyDomain,
            colorDomain: this.candidate.colorDomain,
            length: this.candidate.length,
            volume: this.candidate.volume,
            freezed: this.candidate.freezed,
            secret: this.candidate.secret,
            inscriptionFree: this.candidate.inscriptionFree,
            volunteerFactor: this.candidate.volunteerFactor,
            dependencies: this.candidate.dependencies,
            reverseDependencies: this.candidate.reverseDependencies,
            dependenciesDeep: this.candidate.dependenciesDeep
          }
          // console.log('solution',solution);

          let solutions = [];

          this.solutions.forEach(s => {
            solutions.push(s);
          })
          solutions.push(solution);
          // let solutions = JSON.parse(JSON.stringify(this.solutions));
          // solutions.forEach(solution => {
          //   solution.start = new Date(solution.start);
          //   solution.end = new Date(solution.end);
          //   solution.roomAvailability.startAvailability = new Date(solution.roomAvailability.startAvailability);
          //   solution.roomAvailability.endAvailability = new Date(solution.roomAvailability.endAvailability);
          //   solution.trainerAvailability.startAvailability = new Date(solution.trainerAvailability.startAvailability);
          //   solution.trainerAvailability.endAvailability = new Date(solution.trainerAvailability.endAvailability);
          // })


          let child = new Node(
            this.config,
            this.stake,
            this.resources,
            // this.solutions,
            solutions,
            this.deep + 1);
          // child.solutions.push(solution);
          //child.computeHeurisic();
          childCadidates.push(child);
        })
      } else {

        this.slotDisable = slotDisable;
        // console.log('this.slotDisable',this.slotDisable);
        this.status = 'failed';
      }
      // console.log('phase 4');
      let out = [];
      childCadidates.sort((a, b) => b.heuristic - a.heuristic);
      // console.log(childCadidates.map(o => o.heuristic));
      let i = 0;
      let nbCandidatesSelected = 0
      while (nbCandidatesSelected < this.config.branchCreation && i < childCadidates.length) {
        let closeCandidates = out.filter(o => o.heuristic == childCadidates[i].heuristic);
        if (closeCandidates.length == 0) {
          out.push(childCadidates[i]);
          nbCandidatesSelected++;
        }
        i++;
      }
      // console.log('--',out.map(o => o.solutions.map(s=>s.keyTrainer)));
      return out;
    } else {
      this.status = 'success';
      return [];
    }

  }

  computeHeurisic() {
    // console.log('computeHeurisic',this.candidate.id);
    this.heuristicDetail = {};
    this.heuristicDetail.progress = (this.deep) / this.stake.length;

    this.heuristicDetail.cleanPlacement = this.solutions.filter(s => s.freezed == undefined).reduce((accumulator, currentValue) => {
      // console.log(currentValue.roomAvailability);
      let clean = currentValue.start.getTime() == currentValue.roomStartAvailability.getTime();
      return accumulator + (clean == true ? 1 : 0);
    }, 0);

    this.heuristicDetail.cleanPlacement = this.heuristicDetail.cleanPlacement / this.solutions.length;

    // let minStart;
    // let maxEnd;
    // this.solutions.forEach(s => {
    //   if ((minStart == undefined) || (s.start.getTime() < minStart.getTime())) {
    //     minStart = s.start;
    //   }
    //   if ((maxEnd == undefined) || (s.end.getTime() > maxEnd.getTime())) {
    //     maxEnd = s.end;
    //   }
    // });
    // this.heuristicDetail.totalLengthRatio = 1 - ((maxEnd - minStart) / this.roomAvailabilityLength);

    let solutionsWithReverseDependencies = this.solutions.filter(s => {
      return s.reverseDependencies != undefined && s.reverseDependencies.length > 0;
    })

    let moyStar = (solutionsWithReverseDependencies.map(s => s.start.getTime() - this.roomAvailabilityStart).reduce((accumulator, currentValue) => accumulator + currentValue, 0) / solutionsWithReverseDependencies.length) / this.roomAvailabilityLength;
    moyStar = 1 - moyStar;
    this.heuristicDetail.moyStar = moyStar;


    let truncatedRoomAvailabilityStart = new Date(this.roomAvailabilityStart.getFullYear(), this.roomAvailabilityStart.getMonth() + 1, this.roomAvailabilityStart.getDate());
    let truncatedRoomAvailabilityEnd = new Date(this.roomAvailabilityEnd.getFullYear(), this.roomAvailabilityEnd.getMonth() + 1, this.roomAvailabilityEnd.getDate());
    let currentDate = truncatedRoomAvailabilityStart;
    // console.log('truncatedRoomAvailability', truncatedRoomAvailabilityStart, truncatedRoomAvailabilityEnd);


    let scattering;

    let solutionsWithoutReverseDependencies = this.solutions.filter(s => {
      return s.keyDomain!='BENEVOL' && ( s.reverseDependencies == undefined || s.reverseDependencies.length == 0);
    })

    let trainingCounts = [];
    solutionsWithoutReverseDependencies.forEach(s => {
      // console.log(s);
      let truncateDate = new Date(s.start.getFullYear(), s.start.getMonth() + 1, s.start.getDate());
      let trainingCountsExisting = trainingCounts.filter(tce => tce.day - truncateDate == 0);
      if (trainingCountsExisting.length > 0) {
        trainingCountsExisting[0].counter = trainingCountsExisting[0].counter + 1;
      } else {
        trainingCounts.push({
          day: truncateDate,
          counter: 1
        })
      }
    })
    let minTraining = Infinity;
    let maxTraining = 0;
    trainingCounts.forEach(tc => {
      minTraining = Math.min(tc.counter, minTraining);
      maxTraining = Math.max(tc.counter, maxTraining);
    })
    scattering = this.solutions.length == 0 ? 1 : 1 - ((maxTraining - minTraining) / solutionsWithoutReverseDependencies.length);



    this.heuristicDetail.scattering = scattering;
    // throw new Error('fake');


    // let freeTime = this.resources.trainer.map(trainer => {
    //   let totalTrainerTime = this.solutions.filter(s => s.keyTrainer == trainer.key).reduce((accumulator, currentValue) => accumulator + (currentValue.end.getTime() - currentValue.start.getTime()), 0)
    //   let attendance = this.resources.trainerAvailability.filter(t => t.keyTrainer == trainer.key).reduce((accumulator, currentValue) => accumulator + (currentValue.endAvailability.getTime() - currentValue.startAvailability.getTime()), 0)
    //   // console.log(trainer.key,(attendance- totalTrainerTime)/attendance);
    //   return attendance == 0 ? 1 : (attendance - totalTrainerTime) / attendance;
    //
    // })
    // freeTime = freeTime.filter(ft => ft != 1 && ft != NaN);
    // // console.log('--------');
    // // console.log("freeTime",freeTime);
    // let totalFreeTime = freeTime.reduce((accumulator, currentValue) => currentValue + accumulator, 0);
    // // console.log('freetime',freeTime,totalFreeTime);
    // this.heuristicDetail.moyFreeTime = totalFreeTime / freeTime.length;

    // console.log('Solutions length',this.solutions.length);
    // let sameDomain =this.solutions.map(s=>{
    //   let sameTimeSolutions = this.solutions.filter(other=>{
    //       return (!(s.start.getTime() > other.end.getTime() || s.end.getTime() < other.start.getTime())&&s.key!=other.key);
    //   })
    //   // console.log(' sameTimeSolutions',sameTimeSolutions.length);
    //   if(sameTimeSolutions.length>0){
    //     let sameDomainNB=sameTimeSolutions.filter(sts=>(sts.keyDomain==s.keyDomain));
    //     // console.log('  sameTimeDomain',sameDomainNB.length);
    //     return sameDomainNB.length/sameTimeSolutions.length;
    //   }else{
    //     return 0;
    //   }
    // })
    // // console.log(sameDomain);
    // sameDomain=1-(sameDomain.reduce((accumulator, currentValue)=>{return accumulator + currentValue},0)/sameDomain.length);
    // this.heuristicDetail.sameDomain=sameDomain;

    // console.log('heuristicDetail',this.heuristicDetail);
    // this.heuristic = this.heuristicDetail.progress * this.config.heuristic.progress  + this.heuristicDetail.cleanPlacement * this.config.heuristic.cleanPlacement + this.heuristicDetail.totalLengthRatio * this.config.heuristic.totalLengthRatio  + this.heuristicDetail.moyStar * this.config.heuristic.moyStar + this.heuristicDetail.moyFreeTime * this.config.heuristic.moyFreeTime + this.heuristicDetail.sameDomain  * this.config.heuristic.sameDomain;
    this.heuristic = this.heuristicDetail.progress * this.config.heuristic.progress + this.heuristicDetail.cleanPlacement * this.config.heuristic.cleanPlacement + this.heuristicDetail.moyStar * this.config.heuristic.moyStar + this.heuristicDetail.scattering * this.config.heuristic.scattering;
  }

}
