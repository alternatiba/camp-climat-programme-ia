const request = require('request');
const Service = require('./service');
const Launcher = require('./launcher');
const url = 'https://grappe.io/data/api/5d020912c76f56002dd41cb7-data-IA-CC';
const DelayedResponse = require('http-delayed-response');


const express = require("express");
const app = express();

service=new Service()

app.get("/compute", (req, res) => {
  service.runService(url);
  res.send("service runed consult result at ./result")
});
app.get("/computeDirect", (req, res) => {
  let launcher = new Launcher();
  launcher.launch(url).then(result=>{
      res.send(result)
  });
});

app.get("/computeDirectLocal", (req, res) => {
  let launcher = new Launcher();
  let data = require('./5d020912c76f56002dd41cb7-data-IA-CC.json');
  launcher.launchWithData(data).then(result=>{
      res.send(result)
  });
});

app.get("/result", (req, res) => {
  res.send(service.currentState);
});

app.get("/computeLong", (req, res) => {
  request(url, {
    json: true
  }, (err, result, body) => {
    // console.log(result.body);
    const rawData = result.body;
    let solver = new Solver();
    const data = {
      room: rawData.room,
      training: rawData.training,
      roomAvailability: rawData.roomAvailability
    }

    const config = rawData.config;

    var delayed = new DelayedResponse(req, res);

    // delayed.on('done', function (results) {
    //   res.json(results);
    // }).on('cancel', function () {
    //   // slowFunction failed to invoke its callback within 5 seconds
    //   // response has been set to HTTP 202
    //   res.write('sorry, this will take longer than expected...');
    //   res.end();
    // });

    // delayed.json();
    res.writeHead(200, {
       'Content-Type': 'text/event-stream',
       'Cache-Control': 'no-cache',
       'Connection': 'keep-alive'
     });

    let count=0;
    delayed.on('poll', function() {
      // console.log('poll');
    }).on('heartbeat', function() {
      res.write(count.toString());
      //res.write('');
      // res.writeHead(200, {
      //   'Content-Type': 'text/event-stream',
      //   'Cache-Control': 'no-cache',
      //   'Connection': 'keep-alive'
      // });
      console.log('heartbeat ',count++);
    }).on('cancel', function() {
      console.log('cancel');
      res.end();
    }).on('done', function(results) {
      console.log('done');
      res.write(results);
      res.end();
    }).start(1000);

    // delayed.json();
    // delayed.start(100);

    //solver.executeWithCallback(data,config,delayed);
    let promise = solver.promiseExecute(data, config);
    delayed.end(promise)
    // delayed.end(promise)
    //let solution = solver.execute(data,config);
    // console.log(rawData);
    //res.send(solution);
  });

});

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});
