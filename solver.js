const Node = require('./node');
const SlotsGenerator = require('./slotsGenerator');
module.exports = class Solver {
  constructor(mute) {
    this.mute = mute;
  }

  execute(data, config) {
    console.log('start config',config);
    // console.log('start data',data);
    for (let roomAvailability of data.roomAvailability) {
      roomAvailability.startAvailability = new Date(roomAvailability.startAvailability);
      roomAvailability.endAvailability = new Date(roomAvailability.endAvailability);
      roomAvailability.lengthAvailability = (roomAvailability.endAvailability - roomAvailability.startAvailability) / 1000 / 60;
      roomAvailability.nbAvailability = (roomAvailability.lengthAvailability / config.lengthMinuteStep) - 1;
    }

    // console.log(data.trainerAvailability);
    for (let trainerAvailability of data.trainerAvailability) {
      trainerAvailability.startAvailability = new Date(trainerAvailability.startAvailability);
      trainerAvailability.endAvailability = new Date(trainerAvailability.endAvailability);
    }
    // console.log(data.trainerAvailability);

    // for (let training of data.training) {
    //   training.weight = training.volume;
    // }

    // exlude training without trainer
    // let trainingAvaibility = data.trainerAvailability.reduce((accumulator, currentValue) => accumulator.concat(currentValue.affectation), []);
    // trainingAvaibility = [...new Set(trainingAvaibility)];
    // data.training = data.training.filter(t => trainingAvaibility.filter(ta => ta == t.key).length > 0);

    // console.log('roomAvailability',data.roomAvailability);
    // console.log('training',data.training);
    this.roomAvailability = data.roomAvailability;

    let candidates = [];
    let trainingId = 1;
    let slotsGenerator = new SlotsGenerator();
    for (let training of data.training) {
      let slotsCandidates = slotsGenerator.generate(training, data, config);
      candidates=candidates.concat(slotsCandidates);
    }
    // candidates.forEach(c=>c.weight=c.weight* (1 + Math.random() * 1))
    // console.log('candidates',candidates);

    //affect dependencies from training to parts
    let partsWithoutDependencies = candidates.filter(p => p.dependencies == undefined);
    partsWithoutDependencies.forEach(pwd => {
      pwd.dependencies = [];
      if(pwd.trainingDependencies.length>0){
        pwd.trainingDependencies.forEach(td => {
          // console.log('trainingDependencies',pwd.key,td.dependency);
          let partDependencies = candidates.filter(p => p.key==td.dependency);
          // console.log('partDependencies',partDependencies);
          partDependencies.forEach(pd => {
            pwd.dependencies.push({
              dependency: pd.id,
              isOnlyAiDependency: td.isOnlyAiDependency
            })
          })
        })
        // console.log('pwd.dependencies',pwd.dependencies);
      }
    })

    candidates.forEach(p => {
      p.reverseDependencies = [];
      candidates.forEach(searchForReverse => {
        let reverseDependencies = searchForReverse.dependencies.filter(d => d.dependency == p.id);
        if (reverseDependencies.length > 0) {
          // console.log('REVERSE');
          p.reverseDependencies.push({
            dependency: searchForReverse.id,
            isOnlyAiDependency: reverseDependencies[0].isOnlyAiDependency
          })
        }
      });
    })

    candidates.forEach(p => {
      let dependeciesDeep=this.getDependeciesDeep(p,candidates,0);
      p.dependenciesDeep=dependeciesDeep;
      p.weight = (dependeciesDeep *1000000) + (p.length * p.volume) / p.slotsAvaible.length;
    })


    let initalState = [];
    data.frezzedSlot.forEach(fs => {
      let start = new Date(fs.start);
      let end = new Date(fs.end);
      let roomavaibility = data.roomAvailability.filter(ra => {
        return (!(start.getTime() > ra.endAvailability.getTime() || end.getTime() < ra.startAvailability.getTime()) && fs.keyRoom == ra.keyRoom)
      })
      let candidateInput=candidates.filter(c=>c.id==fs.keySlot);
      // console.log('candidateInput',fs.keySlot,candidateInput.length);
      candidateInput=candidateInput[0];
      // if (roomavaibility.length > 0) {
        initalState.push({
          key: fs.keyTraining,
          id: fs.keySlot,
          start: new Date(fs.start),
          end: new Date(fs.end),
          keyRoom: fs.keyRoom,
          keyTrainer: fs.keyTrainer,
          keyDomain: candidateInput==undefined?fs.keyDomain:candidateInput.keyDomain,
          colorDomain: candidateInput==undefined?fs.colorDomain:candidateInput.colorDomain,
          length: candidateInput==undefined?fs.length:candidateInput.length,
          volume: candidateInput==undefined?fs.volume:candidateInput.volume,
          freezed: fs.freezed,
          // roomStartAvailability: roomavaibility[0].startAvailability,
          secret: fs.secret,
          dependencies: fs.dependencies,
          trainingDependencies : fs.trainingDependencies,
          inscriptionFree: fs.inscriptionFree,
          volunteerFactor: fs.volunteerFactor,
          dependenciesDeep: fs.dependenciesDeep,
          reverseDependencies : fs.reverseDependencies,
        })
      // }else {
      //   console.log('freezed not valid no room',fs.keySlot);
      // }
    })

    let skippedId = [];
    // console.log('candidates',candidates.map(c=>c.id));
    console.log('initalState',initalState.map(is=>{return{id:is.id,secret:is.secret,inscriptionFree:is.inscriptionFree}}));
    candidates = candidates.filter(c => initalState.filter(is => is.id == c.id).length == 0);
    // console.log('candidates',candidates.length);
    let notEnoughResources =  candidates.filter(c => c.slotsAvaible.length==0);
    notEnoughResources.forEach(ner=>{
      skippedId.push({id:ner.id,reason:"not enough ressource"});
    })

    candidates = candidates.filter(c => c.slotsAvaible.length>0);

    candidates = candidates.sort((a, b) => (b.weight - a.weight));

    let allNodes = [];
    let allSlotDisable = [];
    let initNode = new Node(config, candidates, data, initalState, 0);
    // initNode.computeHeurisic();
    allNodes.push(initNode);

    let solution;
    let partialSolution;
    let iteration = 0;
    let failedId = [];
    let failedNode =[];


    while (solution == undefined) {
      iteration++;

      if (allNodes.length == 0) {
        if (partialSolution != undefined) {
          solution = partialSolution;
          solution.conclusion = 'partial solution Stop no sucess but no other solution'
        } else {
          solution = {
            conclusion: "none training able to be computed"
          };
        }
      } else {
        allNodes.sort((a, b) => b.heuristic - a.heuristic);
        // console.log(allNodes.map(r=>r.heuristic));
        let firstNode = allNodes.shift();
        let childs = firstNode.takeStep();

        switch (firstNode.status) {
          case 'pending':
            if (partialSolution == undefined || firstNode.deep > partialSolution.deep) {
              partialSolution = firstNode;
            }
            if (this.mute != true) {
              for (let c = 0; c < firstNode.deep; c++) {
                process.stdout.write(" ");
              }
              console.log('* iteration:', iteration, ' stack size:', allNodes.length, ' last id:', firstNode.candidate.id, ' last heuristic:', firstNode.heuristic.toPrecision(2), ' last solution length:', firstNode.solutions.length);
            }
            childs.forEach(c=>{
              if(c.candidate!=undefined){
                let validDeep = this.getValidDeep(candidates, c.deep, failedId,config);
                if(validDeep!=c.deep){
                  // console.log('pending',validDeep,c.deep);
                  let newNode = new Node(c.config, c.stake, c.resources, c.solutions, validDeep)
                  allNodes.push(newNode);
                }else{
                  allNodes.push(c);
                }
              }else{
                allNodes.push(c);
              }
            })
            break;
          case 'success':
            solution = firstNode;
            solution.conclusion = 'full solution'
            break;
          case 'failed':
            if (partialSolution == undefined || firstNode.deep > partialSolution.deep) {
              partialSolution = firstNode;
            }

            for (let c = 0; c < firstNode.deep; c++) {
              process.stdout.write(" ");
            }
            console.log('|| last id:', firstNode.candidate.id);

            allSlotDisable = allSlotDisable.concat(firstNode.slotDisable);
            failedId = failedId.concat(firstNode.candidate.id);
            failedNode.push(firstNode);

            if(allNodes.length==0 && firstNode.deep<firstNode.stake.length){
              skippedId.push({id:firstNode.candidate.id,reason:"too many conflict without alternativs"});
              let rescuedNodes = failedNode.filter(n=>n.deep==firstNode.deep);
              rescuedNodes.forEach(rn=>{
                let newNode = new Node(rn.config, rn.stake, rn.resources, rn.solutions, firstNode.deep+1);
                allNodes.push(newNode);
              })
            }else{
              let validDeep = this.getValidDeep(candidates, firstNode.deep, failedId,config);
              if(validDeep!=firstNode.deep){
                skippedId.push({id:firstNode.candidate.id,reason:"too many conflict with skipped alternativs"});
                let skippedNodes = allNodes.filter(n => n.candidate.id == firstNode.candidate.id);
                skippedNodes.forEach(n => {
                  let newNode = new Node(n.config, n.stake, n.resources, n.solutions, validDeep)
                  allNodes.push(newNode);
                  allNodes.splice(allNodes.indexOf(n), 1);
                  // console.log(n.config, n.stake, n.resources, n.solutions, validDeep);

                })
                // console.log('after',allNodes.length);
              }
            }



            // }
            // console.log('currentSlotDiseable',currentSlotDiseable.length);
            // console.log('slotDisable', JSON.stringify(firstNode.slotDisable));

            break;
          default:
        }

        if (iteration > config.maxIteration) {
        // if (iteration > 2) {
          solution = partialSolution;
          solution.conclusion = 'partial solution Stop because IA Failed'
        }
      }


    }

    let finalSlotDisable = {
      trainer: [],
      room: [],
      dependencyBefore:[],
      dependencyAfter:[],
    };
    allSlotDisable.forEach(currentValue => {
      if (currentValue.conflict == 'trainer') {
        // console.log('CONFLICT currentValue',currentValue);
        let existingDisable = finalSlotDisable.trainer.filter(fds => {
          return fds.id == currentValue.id && fds.keyTrainer == currentValue.keyTrainer && fds.slotConflict == currentValue.slotConflict
        })
        if (existingDisable.length > 0) {
          existingDisable[0].occurence = existingDisable[0].occurence + 1;
          // existingDisable[0].detail.push(currentValue);
        } else {
          finalSlotDisable.trainer.push({
            id: currentValue.id,
            keyTrainer: currentValue.keyTrainer,
            occurence: 1,
            slotConflict:currentValue.slotConflict
          })
        }
      }
      if (currentValue.conflict == 'room') {
        let existingDisable = finalSlotDisable.room.filter(fds => {
          return fds.id == currentValue.id && fds.keyRoom == currentValue.keyRoom && fds.slotConflict == currentValue.slotConflict
        })
        if (existingDisable.length > 0) {
          existingDisable[0].occurence = existingDisable[0].occurence + 1;
          // existingDisable[0].detail.push(currentValue);
        } else {
          finalSlotDisable.room.push({
            id: currentValue.id,
            keyRoom: currentValue.keyRoom,
            occurence: 1,
            slotConflict:currentValue.slotConflict
            // detail:[currentValue]
          })
        }
      }
      if (currentValue.conflict == 'dependencyBefore') {
        let existingDisable = finalSlotDisable.dependencyBefore.filter(fds => {
          return fds.id == currentValue.id && fds.dependencies == currentValue.dependencies
        })
        if (existingDisable.length > 0) {
          existingDisable[0].occurence = existingDisable[0].occurence + 1;
          // existingDisable[0].detail.push(currentValue);
        } else {
          finalSlotDisable.dependencyBefore.push({
            id: currentValue.id,
            dependencies: currentValue.dependencies,
            occurence: 1,
            // detail:[currentValue]
          })
        }
      }
      if (currentValue.conflict == 'dependencyAfter') {
        let existingDisable = finalSlotDisable.dependencyAfter.filter(fds => {
          return fds.id == currentValue.id && fds.reverseDependencies == currentValue.reverseDependencies
        })
        if (existingDisable.length > 0) {
          existingDisable[0].occurence = existingDisable[0].occurence + 1;
          // existingDisable[0].detail.push(currentValue);
        } else {
          finalSlotDisable.dependencyAfter.push({
            id: currentValue.id,
            reverseDependencies: currentValue.reverseDependencies,
            occurence: 1,
            // detail:[currentValue]
          })
        }
      }
    })


    let out = {
      solution: solution.solutions,
      conclusion: solution.conclusion,
      config: config,
      data: data,
      initalState: initalState,
      finalSlotDisable: finalSlotDisable,
      skippedId:skippedId,
    };
    console.log('conclusion',solution.conclusion);
    // console.log('solutions',solution.solutions);
    console.log('finalSlotDisable',finalSlotDisable);
    // console.log('***** trainers conflicts*******');
    // finalSlotDisable.trainer.forEach(fsdT=>{
    //   console.log(fsdT);
    // })
    console.log('***** skippedId *****',skippedId);


    // console.log(solution.solutions != undefined ? 'sucess' : solution);
    console.log('iteration number', iteration);
    console.log('last deep', solution.deep);
    console.log('last heuristic', solution.heuristic);
    console.log('last heuristicDetail', solution.heuristicDetail);

    return out;
  }

  getValidDeep(candidates, currentDeep, failedId,config) {
    // console.log('getValidDeep',candidates, currentDeep, failedId);
    let out = currentDeep;
    let solution = false;
    while (solution == false) {
      //console.log('increment',out,candidates.length);
      let EverAbandonedId = failedId.filter(f => f == candidates[out].id);
      // console.log('EverAbandonedId',EverAbandonedId.length);
      if (EverAbandonedId.length > config.skippedLimit) {
        out++;
      } else {
        solution = true;
      }
    }
    return out;
  }

  getDependeciesDeep(candidate,candidates,deep){
    // console.log('- getDependeciesDeep',candidate.id,deep);
    let currentDeep=deep;
    // console.log(candidate.dependencies);
    candidate.reverseDependencies.forEach(d=>{
      // console.log(d);
      let dependeciesSlot = candidates.filter(c=>c.id==d.dependency);
      // console.log('-- dependeciesSlot',dependeciesSlot);
      let dependecyDeep = this.getDependeciesDeep(dependeciesSlot[0],candidates,deep+1);
      currentDeep = Math.max(currentDeep,dependecyDeep);
    })
    return currentDeep;
  }

  // iterate() {
  //   let currentCadidate = this.initCandites.shift()
  // }



}
