const Solver= require('./solver');
const request = require('request');
module.exports = class Launcher {
  constructor() {

  }

  launch(url,mute){
    return new Promise((resolve,reject)=>{
      console.log(url);
      request(url, {
        json: true
      }, (err, result, body) => {
        // console.log('raw: ',result.body);
        const rawData = result.body;
        // let solver = new Solver();
        const data = {
          training: rawData.training,
          trainer : rawData.trainer,
          roomAvailability: rawData.roomAvailability,
          trainerAvailability: rawData.trainerAvailability,
          frezzedSlot: rawData.frezzedSlot,
        }

        const config = rawData.config;
        let solver = new Solver(mute);
        let out = solver.execute(data,config);
        resolve(out);
      });
    })
  }

  launchWithData(rawData,mute){
        // console.log('raw: ',result.body);
        // const rawData = result.body;
        // let solver = new Solver();
        return new Promise((resolve,reject)=>{
          const data = {
            training: rawData.training,
            trainer : rawData.trainer,
            roomAvailability: rawData.roomAvailability,
            trainerAvailability: rawData.trainerAvailability,
            frezzedSlot: rawData.frezzedSlot,
          }

          const config = rawData.config;
          let solver = new Solver(mute);
          let out = solver.execute(data,config);
          resolve(out);

        });
  }
}
