module.exports = class SlotsGenerator {
  constructor(mute) {
    this.mute = mute;
  }

  generate(candidate, resources, config) {
    let roomAvailabilityCandidates = resources.roomAvailability.filter(r => r.volume >= candidate.volume);
    //console.log('roomAvailabilityCandidates',roomAvailabilityCandidates);

    // if(candidate.key=='COM25'){
    //   console.log('roomAvailabilityCandidates before room filter',roomAvailabilityCandidates.length);
    // }
    if (candidate.roomAffectation.length > 0) {
      roomAvailabilityCandidates = roomAvailabilityCandidates.filter(ra => {
        let roomValidity = false;
        candidate.roomAffectation.forEach(roomAffectation => {
          if (roomAffectation == ra.keyRoom) {
            roomValidity = true;
          }
        })
        return roomValidity;
      })
    }

    // if(candidate.key=='COM25'){
    //   console.log('roomAvailabilityCandidates after room filter',roomAvailabilityCandidates.length);
    // }

    if (candidate.equipment.length>0){
        // console.log("equipment");

        roomAvailabilityCandidates = roomAvailabilityCandidates.filter(ra => {
          let validEquipment=0;
          let roomValidity = false;
          candidate.equipment.forEach(trainingEquipment => {
            // console.log('trainingEquipment',trainingEquipment);
            let roomEquimentSearch = ra.equipment.filter(e=>e==trainingEquipment.key);
            if(roomEquimentSearch.length>0){
              validEquipment++;
            }
          })
          // console.log(validEquipment,candidate.equipment.length);
          return validEquipment==candidate.equipment.length;
        })
        // console.log('roomAvailabilityCandidates',roomAvailabilityCandidates);
    }

    // if(candidate.key=='COM25'){
    //   console.log('roomAvailabilityCandidates after equipement filter',roomAvailabilityCandidates.length);
    // }
    // console.log('roomAvailabilityCandidates',candidate.key,roomAvailabilityCandidates.length);
    roomAvailabilityCandidates.sort((a, b) => a.volume - b.volume);
    // let index = 0;
    // let roomAvailabilityChoose = roomAvailabilityCandidates[index];

    // let lastSameTrainingOccurence = this.solutions.filter(s => s.key == this.candidate.key).sort((a, b) => b.occurence > a.occurence)[0];


    // console.log('phase 2');

    // console.log('slotAvailability',candidate.key,slotAvailability.length);
    // console.log('slotAvailability',slotAvailability);
    // return slotAvailability;
    let parts = []



    if (candidate.slots.length > 0) {
      for (let slot of candidate.slots) {

        let myRegexp = /.*\[(.*)\]\[(.*)\]/g;
        let match = myRegexp.exec(slot.keySlot);

        let part={
          id: candidate.key + "[" + match[1] + "][" + match[2] + "]",
          // id: training.key + "[" + occurence + "]",
          occurence: match[1],
          part: match[2],
          key: candidate.key,
          keyDomain: candidate.keyDomain,
          colorDomain: candidate.colorDomain,
          dependencies: slot.dependencies.length > 0 ? slot.dependencies : undefined,
          length: slot.length != undefined ? slot.length : candidate.length,
          volume: candidate.volume,
          title: candidate.title,
          ordered: candidate.ordered,
          spacing: candidate.spacing,
          multi: candidate.multi,
          volunteerFactor: candidate.volunteerFactor,
          inscriptionFree: candidate.inscriptionFree,
          secret: candidate.secret,
          trainingDependencies: candidate.dependencies,
        }

        let slotAvailability
        if (slot.trainerAffectation.length > 0) {
          // if(candidate.key=='COM25'){
          //   console.log('generateSlotAvailability with trainer');
          // }

          slotAvailability = this.generateSlotAvailability(roomAvailabilityCandidates, resources, config, part, slot.trainerAffectation);
        } else {
          // if(candidate.key=='COM25'){
          //   console.log('generateSlotAvailability without trainer');
          // }

          slotAvailability = this.generateSlotAvailability(roomAvailabilityCandidates, resources, config, part);
        }

        // if(candidate.key=='COM25'){
        //   console.log('slotAvailability',slotAvailability.length);
        // }
        part.slotsAvaible = slotAvailability;

        // console.log('regex match slot',match);
        // console.log('slotAvailability',candidate.key + "[" + match[1] + "]["+match[2]+"]", slotAvailability.length);
        parts.push(part);
      }
    } else {
      let slotAvailability = this.generateSlotAvailability(roomAvailabilityCandidates, resources, config, candidate);

      // if(candidate.key=='COM25'){
      //   console.log('slotAvailability',slotAvailability.length);
      // }

      let occurence = 1;
      for (let i = 0; i < candidate.occurence; i++) {
        // console.log('slotAvailability',candidate.key + "[" + occurence + "][1]", slotAvailability.length);
        parts.push({
          id: candidate.key + "[" + occurence + "][1]",
          // id: training.key + "[" + occurence + "]",
          occurence: occurence,
          part: 1,
          key: candidate.key,
          keyDomain: candidate.keyDomain,
          colorDomain: candidate.colorDomain,
          length: candidate.length,
          volume: candidate.volume,
          slotsAvaible: slotAvailability,
          title: candidate.title,
          ordered: candidate.ordered,
          spacing: candidate.spacing,
          multi: candidate.multi,
          volunteerFactor: candidate.volunteerFactor,
          inscriptionFree: candidate.inscriptionFree,
          secret: candidate.secret,
          weight: (candidate.length * candidate.length) / slotAvailability.length,
          trainingDependencies: candidate.dependencies,
        });
        occurence++;
      }
    }

    // let inspect=parts.filter(p=>p.id=="COM25[1][1]");
    // if(inspect.length>0){
    //   console.log('inspect length',inspect[0].slotsAvaible.length);
    //   // console.log('inspect',inspect[0].slotsAvaible);
    // }

    return parts;

  }

  generateSlotAvailability(roomAvailabilityCandidates, resources, config, candidate, trainers) {

    let slotAvailability = [];
    // if(candidate.key=='COM25'){
    //   console.log('trainers',trainers);
    //   console.log(candidate);
    // }
    roomAvailabilityCandidates.forEach(r => {

      let startRoomTime = r.startAvailability;
      let endRoomTime = r.endAvailability;
      let lengthRoomTime = (endRoomTime - startRoomTime) / 1000 / 60;
      let slotStepNb = (lengthRoomTime / 30);
      for (let startStep = 0; startStep < slotStepNb; startStep++) {

        let startStepTime = new Date(startRoomTime.getTime() + (startStep * config.lengthMinuteStep * 60 * 1000));
        let endStepTime = new Date(startStepTime.getTime() + (candidate.length * 60 * 1000));
        if (endStepTime <= endRoomTime) {
          if (trainers == undefined || trainers.length == 0) {
            // if(candidate.key=='COM25'){
            //   console.log('resources.trainerAvailability',resources.trainerAvailability.length);
            // }
            let trainerAvailabilityValid = resources.trainerAvailability.filter(ta =>
              ta.affectation.filter(a =>
                a == candidate.key
              ).length > 0 &&
              ta.startAvailability.getTime() <= startStepTime.getTime() && ta.endAvailability.getTime() >= endStepTime.getTime()
            );
            // if(candidate.key=='COM25'){
            //   console.log('trainerAvailabilityValid without trainer',trainerAvailabilityValid.length);
            // }

            // console.log("trainerAvailabilityValid + roomAvailability",trainerAvailabilityValid.length,r.keyRoom,startStepTime,endStepTime);
            trainerAvailabilityValid.forEach(ta => {
              slotAvailability.push({
                startStepTime: startStepTime,
                endStepTime: endStepTime,
                keyRoom: r.keyRoom,
                keyTrainer: [ta.keyTrainer],
                roomAvailability: r,
                trainerAvailability: [ta],
                roomStartAvailability: r.startAvailability
              });
            });
          } else {

            let trainerAvailabilityValid = resources.trainerAvailability.filter(ta => {
              let trainerValid = false;
              for (let trainer of trainers) {
                if (trainer == ta.keyTrainer) {
                  trainerValid = true;
                }
              }
              return trainerValid && ta.startAvailability.getTime() <= startStepTime.getTime() && ta.endAvailability.getTime() >= endStepTime.getTime()
            });
            // if(candidate.key=='COM25'){
            //   console.log('trainerAvailabilityValid',trainerAvailabilityValid.length,trainerAvailabilityValid.map(ta=>ta.keyTrainer),startStepTime, endStepTime);
            // }
            // console.log("trainerAvailabilityValid comparaison",trainerAvailabilityValid.length,trainers.length);
            if (trainerAvailabilityValid.length == trainers.length) {
              // console.log('MATCH!!!!', candidate.key);
              slotAvailability.push({
                startStepTime: startStepTime,
                endStepTime: endStepTime,
                keyRoom: r.keyRoom,
                keyTrainer: trainers,
                roomAvailability: r,
                trainerAvailability: trainerAvailabilityValid,
                roomStartAvailability: r.startAvailability
              });
            } else {
              // console.log('Not MATCH :-(', candidate.key);
            }
          }

        }
      }
    });
    // console.log(candidate.key,slotAvailability.length);
    return slotAvailability;
  }
}
