const {
  workerData,
  parentPort
} = require('worker_threads')

const Launcher= require('./launcher')
// You can do any heavy stuff here, in a synchronous way
// without blocking the "main thread"
let launcher = new Launcher();
launcher.launch(workerData,true).then(result=>{
  parentPort.postMessage({
    state: 'solution',
    stateDesc: 'solution finded by IA',
    data : result
  });
})
