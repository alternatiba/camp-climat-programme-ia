const { Worker } = require('worker_threads');
module.exports = class Service {
  constructor(){
    this.states = [];
    this.currentState={status:'initialisation',statusDesc:'service initialise'}
  }



  runService(workerData) {
    return new Promise((resolve, reject) => {
      const worker = new Worker('./worker.js', { workerData });
      worker.on('message', (state)=>{
          this.states.push(state);
          this.currentState=state;
      });
      worker.on('error', reject);
      worker.on('exit', (code) => {
        console.log('END');

        if (code !== 0){
          reject(new Error(`Worker stopped with exit code ${code}`));
        }else {
          resolve(this.states);
        }
      })
    })
  }
}
